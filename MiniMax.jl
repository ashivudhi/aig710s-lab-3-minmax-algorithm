### A Pluto.jl notebook ###
# v0.14.3
#Ashivudhi Erro Remaider

using Markdown
using InteractiveUtils

# ╔═╡ facdb176-ade7-11eb-33d6-d199cd74317e
md"# Minimax Algorithm" 

# ╔═╡ 3f200190-2731-4121-b9d8-ef1024315670
md"### Nodes Definion" 

# ╔═╡ f87042c1-fbcf-4475-9287-c4a61ed68ce4
struct TerminalNode
	utility::Int64
end

# ╔═╡ 0215ee11-88e9-4542-8b68-f6b78f13aebf
@enum PlayerAction Pmin Pmax

# ╔═╡ 63ad9ddc-8aee-4ba9-a6b5-67dd9d337bee
struct PlayerNode
	action::PlayerAction
	nodes::Vector{Union{TerminalNode,PlayerNode}}
end

# ╔═╡ 62eb78b8-7932-45ce-8743-c9059f44bdc8
md"### Solve the game" 

# ╔═╡ f39396c9-0bc4-4742-b012-01ac21c900b7
struct Game
	game_initial::PlayerNode
end

# ╔═╡ 3eba6ee4-5d39-4233-889b-bbd2bd72671c
function evaluate_node(node::TerminalNode)
	return node.utility
end

# ╔═╡ 811713a1-077c-46a3-9217-e928b80fc206
function evaluate_node(node::PlayerNode)
	if node.action==Pmin
		minimum(map(single_node->evaluate_node(single_node),node.nodes))
	else
		maximum(map(single_node->evaluate_node(single_node),node.nodes))
	end
end

# ╔═╡ 475a6921-f89b-4970-ab8c-601057959383
n2 =PlayerNode(Pmin,[TerminalNode(3),TerminalNode(5),TerminalNode(10)])

# ╔═╡ cb1a2593-d293-40a9-8867-0c0f277d3871
n3 =PlayerNode(Pmin,[TerminalNode(2),TerminalNode(8),TerminalNode(19)])

# ╔═╡ ce1c34ce-b2f2-4dd3-9f6c-8e18cc57ccf7
n4 =PlayerNode(Pmin,[TerminalNode(2),TerminalNode(7),TerminalNode(3)])

# ╔═╡ 93451cf5-97f5-4b6c-a74d-3b275e7e83e9
n1 = PlayerNode(Pmax,[n2,n3,n4])

# ╔═╡ 8b1e964c-7ca1-4439-be91-138aca9d8dec
the_game = Game(n1)

# ╔═╡ bf88c7dc-7f15-48c7-8965-477dde92d46d
function solve_game(game::Game)
	evaluate_node(game.game_initial)
end

# ╔═╡ b1ef4588-dfef-4c21-9132-a1a19ea45820
solve_game(the_game)

# ╔═╡ Cell order:
# ╠═facdb176-ade7-11eb-33d6-d199cd74317e
# ╠═3f200190-2731-4121-b9d8-ef1024315670
# ╠═f87042c1-fbcf-4475-9287-c4a61ed68ce4
# ╠═0215ee11-88e9-4542-8b68-f6b78f13aebf
# ╠═63ad9ddc-8aee-4ba9-a6b5-67dd9d337bee
# ╠═62eb78b8-7932-45ce-8743-c9059f44bdc8
# ╠═f39396c9-0bc4-4742-b012-01ac21c900b7
# ╠═3eba6ee4-5d39-4233-889b-bbd2bd72671c
# ╠═811713a1-077c-46a3-9217-e928b80fc206
# ╠═475a6921-f89b-4970-ab8c-601057959383
# ╠═cb1a2593-d293-40a9-8867-0c0f277d3871
# ╠═ce1c34ce-b2f2-4dd3-9f6c-8e18cc57ccf7
# ╠═93451cf5-97f5-4b6c-a74d-3b275e7e83e9
# ╠═8b1e964c-7ca1-4439-be91-138aca9d8dec
# ╠═bf88c7dc-7f15-48c7-8965-477dde92d46d
# ╠═b1ef4588-dfef-4c21-9132-a1a19ea45820
